<meta name="theme-color" content="#2F3BA2"/>
<title>Bb+a</title>
  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="resource-type" content="document" />

    <link rel="manifest" href="manifest.json">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Bb+a">
    <link rel="apple-touch-icon" href="touch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/icon-76x76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/icon-120x120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/icon-152x152.png">    
    <link rel="apple-touch-icon" sizes="192x192" href="/img/icon-192x192.png">    
    

    <meta name="title" content="Bb+a">
    <meta name="description" content="Bb+a" />
    <meta name="keywords" content="Bb+a">
    <meta name="robots" content="all, index, follow" />
    <meta name="googlebot" content="all, index,follow">
    <meta name="author" content="https://apterynet.net/bba">
    <meta name="rating" content="General">    
    <meta name="owner" content="Bb+a" />
    <meta name="fragment" content="!"/>
    <meta name="subject" content="Bb+a">
    <meta name="distribution" content="Global">
    <meta name="abstract" content="Bb+a">
    <meta name="copyright" content="https://apterynet.net/bba">

    <meta property="og:title" content="Bb+4"/>
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://apterynet.net/bba"/>
    <meta property="og:image" content="https://apterynet.net/bba/logo.png"/>
    <meta property="og:description" content="Bb+a"/>
    <meta property="og:determiner" content="auto"/>
    <meta property="og:locale" content="es_MX">
    <meta property="og:locale:alternate" content="es_MX" />
    <meta property="og:site_name" content="Bb+a"/>

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Bb+a" />
    <meta name="twitter:site" content="@Bba" />
    <meta name="twitter:title" content="Bb+4" />
    <meta name="twitter:creator" content="Bb+a" />
    <meta name="twitter:url" content="https://apterynet.net/bba" />
    <meta name="twitter:image" content="https://apterynet.net/bba/logo.png" />

    <link rel="author" href="https://apterynet.net/bba">
    <link rel="alternate" href="https://apterynet.net/bba" hreflang="es-mx">
    <link rel="license" href="https://apterynet.net/bba" hreflang="es-mx">
    <link rel="help" href="https://apterynet.net/bba">
    <link rel="canonical" href="https://apterynet.net/bba">
    <link rel="dns-prefetch" href="https://apterynet.net/bba">    

    <link rel="preload" href="css/styles-generic.css" as="style">
    <link rel="preload" href="js/app.js" as="script">
    <link rel="preload" href="js/modernizr.js" as="script">

    <link href="css/styles-generic.css" rel="stylesheet">   

    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script type="application/ld+json">
        {
        "@context":"https://schema.org",
        "@type":"WebPage",
        "name": "Bb+a",
        "url":"https://apterynet.net/bba",
        "description": "Bb+a",
        "keywords": "Bb+a"
        }
    </script>


