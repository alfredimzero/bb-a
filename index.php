<!DOCTYPE html>
<html lang="es-mx" prefix="og: http://ogp.me/ns#">
   <head>
      <?php include('includes/head.php') ?>
   </head>
   <body>
      <?php include('includes/header.php') ?>    
      <main>
         <section class="">
            <div class="container">
                  <div class="form-row" id="zero-page">
                        <div class="col-12 col-sm-4 offset-sm-4">
                           <h2 class="title-app text-center" >Bienvenidos</h2>
                           <form>
                              <div class="form-group">
                                    <button id="iniciar" type="button" class="btn btn-primary">Iniciar</button>
                              </div>                                                          
                           </form>
                        </div>
                     </div>
               <div class="form-row d-none" id="first-page">
                  <div class="col-12 col-sm-4 offset-sm-4">
                     <h2 class="title-app text-center" >Transferencia a otros bancos</h2>
                     <form>
                        <div class="form-group">
                           <label for="exampleFormControlSelect1">Cuenta Origen</label>
                           <input type="text" class="form-control" id="cuentaOrigen" placeholder="cuenta origen">
                        </div>
                        <div class="form-group">
                           <label for="exampleFormControlSelect1">Cuenta Destino</label>
                           <input type="text" class="form-control" id="cuentaDestino" placeholder="cuenta origen">
                        </div>
                        <div class="form-group">
                           <label for="exampleFormControlInput1">Monto</label>
                           <input type="text" class="form-control" id="monto" placeholder="monto">
                        </div>
                        <div class="form-group">
                           <label for="exampleFormControlInput1">Concepto</label>
                           <input type="text" class="form-control" id="concepto" placeholder="concepto">
                        </div>
                     </form>
                  </div>
               </div>
               
               <div class="form-row d-none" id="second-page">
                  <div class="col-12 col-sm-4 offset-sm-4">
                     <h2 class="title-app text-center" >Confirmar Datos</h2>
                     <div class="form-group">
                        <label for="exampleFormControlSelect1">Cuenta Origen: <span>123456789</span> </label>                                         
                     </div>
                     <div class="form-group">
                        <label for="exampleFormControlSelect1">Cuenta Destino: <span>987654321</span> </label>                                         
                     </div>
                     <div class="form-group">
                        <label for="exampleFormControlInput1">Monto:<span>$1,000.00</span> </label>                                          
                     </div>
                     <div class="form-group">
                        <label for="exampleFormControlInput1">Concepto: <span>Pago de servicios</span> </label>                                          
                     </div>
                  </div>
               </div>
               
               <div class="form-row d-none " id="third-page">
                  <div class="col-12 col-sm-4 offset-sm-4">
                     <h2 class="title-app text-center" >Listo</h2>
                  </div>
               </div>
            </div>
         </section>
      </main>
      <?php include('includes/js.php') ?>    
      <script>
         if ('serviceWorker' in navigator) {
         window.addEventListener('load', () => {
         navigator.serviceWorker.register('service-worker.js')
         .then((reg) => {
          console.log('Service worker registered.', reg);
         });
         });
         }
      </script>
   </body>
</html>